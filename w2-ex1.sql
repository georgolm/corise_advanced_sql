-- +++++++++++++++++++++++++++++++++
-- CTEs for main Query
-- +++++++++++++++++++++++++++++++++ 


with chicago_geoloc as ( 

-- get geo locations from Chicago, IL

    select 
        geo_location
    from vk_data.resources.us_cities 
    where city_name = 'CHICAGO' and state_abbr = 'IL'), 

gary_geoloc as ( 

-- get geo locations from Gary, IN

    select 
        geo_location
    from vk_data.resources.us_cities 
    where city_name = 'GARY' and state_abbr = 'IN'), 

c_survey as (

-- Get the number of food preferences per user

    select 
        customer_id,
        count(*) as food_pref_count
    from vk_data.customers.customer_survey
    where is_active = true
    group by 1
)

-- +++++++++++++++++++++++++++++++++
-- Main Query
-- +++++++++++++++++++++++++++++++++ 

-- select necessary information and calculate the distance to chicago and gary
select 
    first_name || ' ' || last_name as customer_name,
    c_adress.customer_city,
    c_adress.customer_state,
    c_survey.food_pref_count,
    (st_distance(us.geo_location, chicago_geoloc.geo_location) / 1609)::int as chicago_distance_miles,
    (st_distance(us.geo_location, gary_geoloc.geo_location) / 1609)::int as gary_distance_miles
from vk_data.customers.customer_address as c_adress

-- Combine with customer data 
inner join vk_data.customers.customer_data as c_data 
    on c_adress.customer_id = c_data.customer_id

-- Combine information on geolocation
left join vk_data.resources.us_cities as us 
    on lower(trim(c_adress.customer_state))) = lower(trim(us.state_abbr))
        and lower(trim(c_adress.customer_city)) = lower(trim(us.city_name))

-- Combine with user preferences
inner join  c_survey 
    on c_data.customer_id = c_survey.customer_id

-- Add geolocation about chicago and gary to every row
cross join chicago_geoloc
cross join gary_geoloc

-- Filter necessay city, but only in its corresponding state
where (customer_state = 'KY'
        and trim(city_name) in ('concord', 'georgetown', 'ashland')) 
    or (customer_state = 'CA' 
        and trim(city_name) in ('oakland', 'pleasant hill'))
    or (customer_state = 'TX' 
        and trim(city_name) in ('arlington', 'brownsville'))

