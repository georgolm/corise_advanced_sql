

-- My query finished after 538ms and returned 20 rows. 
-- I have one node, that is considered most expensive. It points to the following commands: 

-- Attributes

-- Grouping Keys
EXTRACT(day from WEBSITE_ACTIVITY.EVENT_TIMESTAMP)
EXTRACT(month from WEBSITE_ACTIVITY.EVENT_TIMESTAMP)

-- Aggregate Functions
COUNT(DISTINCT WEBSITE_ACTIVITY.SESSION_ID)
COUNT(IFF(CONTAINS(WEBSITE_ACTIVITY.EVENT_DETAILS, '"event":"search"'), 1, null))
COUNT(IFF(WEBSITE_ACTIVITY.EVENT_DETAILS LIKE '%"event":"view_recipe"%', 1, null))
MODE(REGEXP_SUBSTR(WEBSITE_ACTIVITY.EVENT_DETAILS, '[[:alnum:]]{8}-[[:alnum:]]{4}-[[:alnum:]]{4}-[[:alnum:]]{4}-[[:alnum:]]{12}'))

-- The most expensive operation appears to be aggregation of events, extracted from JSON Text. Maybe it makes sense to choose another format here or a supplementary event table, we can join to. 
-- The query is not yet adjusted for higher volumes, but you could adress volumne problem, by specifiying the questions in te report. 
    -- E.g. only considering user, that viewed a recipe and ignore thoses who just search, or vice versa. 
    -- Searching through JSON Text seems to be expensive and could be solved, by reading JSON-Text into an extra table, besides the usual statements and save time, when calling the report. 


with session_length as(
-- calculate the length of an individual session

    select session_id,
    	min(event_timestamp) as earliest_timestamp, 
        max(event_timestamp) as latest_timestamp, 
        datediff(second,earliest_timestamp, latest_timestamp) as duration, 
        day(earliest_timestamp) as event_day,
        month(earliest_timestamp) as event_month,
        year(earliest_timestamp) as event_year
    from vk_data.events.website_activity
    group by session_id
    order by event_month, event_day, duration

), session_length_per_day as(
-- summarise the number of sessions and their durations per day

    select count(1) as no_sessions, 
    	sum(duration) / no_sessions as mean_session_length,
    	event_day,
    	event_month,
    	event_year
    from session_length
    group by event_day, event_month, event_year

), view_summary as(
-- summarise how many searches and recipe views happened per day

    select 
    	day(event_timestamp) as event_day,
    	month(event_timestamp) as event_month,
    	year(event_timestamp) as event_year,
    	count(distinct session_id) as no_unique_sessions, 
        count(case when event_details like '%"event":"search"%' then 1 else null end) as no_searches,
        count(case when event_details like '%"event":"view_recipe"%' then 1 else null end) as no_view_recipes,
        div0null(no_searches, no_view_recipes) as mean_search_per_view,
        mode(regexp_substr(event_details, '[[:alnum:]]{8}-[[:alnum:]]{4}-[[:alnum:]]{4}-[[:alnum:]]{4}-[[:alnum:]]{12}')) as most_viewed_recipe
    from vk_data.events.website_activity
    group by event_day, event_month, event_year
) 

-- select values and join information on session length and session summary

select view_summary.event_day,
	view_summary.event_month,
    view_summary.event_year,
    view_summary.no_unique_sessions,
	session_length_per_day.mean_session_length,
    view_summary.mean_search_per_view,
    view_summary.most_viewed_recipe
from view_summary left join session_length_per_day
on view_summary.event_day = session_length_per_day.event_day
	and view_summary.event_month = session_length_per_day.event_month
	and view_summary.event_year = session_length_per_day.event_year
order by event_year, event_month, event_day
