-- Corise Advanced SQL, Week 1, Exercise 1


with unique_cities as(
-- Getting unique city information
    
    SELECT 
        UPPER(TRIM(city_name)) as city_name,
        UPPER(TRIM(state_abbr)) as state_abbr,
        GEO_LOCATION as geoloc
    FROM RESOURCES.US_CITIES
    qualify row_number() over (partition by city_name, state_abbr order by 1) = 1
), 

supplier_geoloc as(
-- Assign geolocation to suppliers
    
    SELECT SUPPLIER_NAME as s_name,
        SUPPLIER_ID as s_id,
        UPPER(TRIM(SUPPLIER_CITY)) as s_city,
        UPPER(TRIM(SUPPLIER_STATE)) as s_state,
        geoloc
    FROM SUPPLIERS.SUPPLIER_INFO
    LEFT JOIN unique_cities
    ON s_city = city_name AND s_state = state_abbr

), 

customer_geoloc as(
-- Assign geolocation to customers  
    
    SELECT distinct
        UPPER(TRIM(CUSTOMER_CITY)) as c_city,
        UPPER(TRIM(CUSTOMER_STATE)) as c_state,
        geoloc
    FROM CUSTOMERS.CUSTOMER_ADDRESS
    LEFT JOIN unique_cities
    ON c_city = city_name AND c_state = state_abbr

), customer_supplier_loc as(
-- Combine customer and supplier geoinformation
-- Calculate distance between supplier and customer and keep the shortest
    
    SELECT 
        c_city, 
        c_state, 
        s_id,
        s_name, 
        s_city, 
        s_state,
        ST_DISTANCE(customer_geoloc.geoloc,supplier_geoloc.geoloc) / 1000 AS distance_km 
    FROM customer_geoloc, supplier_geoloc
    qualify row_number() over (partition by c_city, c_state order by distance_km) = 1
    
), closest_supplier as (
-- Combine infromation on closest supplier to customer data
    
    SELECT 
        CUSTOMER_ID as c_id,
        CUSTOMER_CITY,
        CUSTOMER_STATE, 
        s_id, 
        s_name, 
        distance_km
    FROM CUSTOMERS.CUSTOMER_ADDRESS
    LEFT JOIN customer_supplier_loc
    ON UPPER(TRIM(CUSTOMER_CITY)) = c_city AND UPPER(TRIM(CUSTOMER_STATE)) = c_state
    
), exercise1_output as (
-- create exercise 1 tables

    SELECT
        CUSTOMER_ID, 
        FIRST_NAME, 
        LAST_NAME, 
        EMAIL, 
        s_id,
        s_name, 
        distance_km
    FROM CUSTOMERS.CUSTOMER_DATA
    LEFT JOIN closest_supplier 
    ON CUSTOMER_ID = c_id
)

--- Show the final results for all entries, where a distance can be calculated
SELECT *
FROM exercise1_output
WHERE distance_km IS NOT NULL




